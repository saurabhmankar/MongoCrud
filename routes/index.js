var express = require('express');
var router = express.Router();
const Window = require('window');
let jwt = require('jsonwebtoken');
var token = jwt.sign({ foo: 'bar' }, 'shhhhh');

// import conf from './config';
 
const window = new Window();
//var document=require('document');
var fs = require('fs');
var bodyParser = require('body-parser');
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
//Requiring mongoose
var mongoose = require("mongoose");
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/mongodbtest", { useNewUrlParser: true });
//defining schema
var nameSchema = new mongoose.Schema({
  game: String,
  publisher: String,
  released: Date,
  rating: Number,
  score: Number
});

var LoginSchema = new mongoose.Schema({
  username: String,
  password: String,
});

var RegisterSchema = new mongoose.Schema({
  email: String,
  password: String,
});

var User = mongoose.model("User", nameSchema);
var Login=mongoose.model("Login",LoginSchema);
var Register=mongoose.model("Register",RegisterSchema);


router.post('/login', function (req, res, next) {
  var data=new Login(req.body);
  console.log(data);
  var email=data.username;
  Register.find({ "email": email }, function (err, data) {
    res.send(data);
    console.log("Data Found :"+ data);
    // console.log("Search Result:")
    // console.log("Email:"+data[0].email);
    // console.log("Password:"+data[0].password);

  })
    


  });
  



router.post('/register', function (req, res, next) {
  var data=new Register(req.body);
  data.save()
  .then(item => {
    //res.json("item saved to database");
  
    jwt.sign({data},'secretkey',(err,token)=>{
      res.json({token});
    });
  })
  .catch(err => {
    res.status(400).send("unable to save to database");
  });
  

});



//Inserting data in database,after insert check in the gamelist
router.post("/adddetails", (req, res) => {
  var myData = new User(req.body);
  myData.save()
    .then(item => {
      // res.send("item saved to database");
      res.redirect('/list');
      res.json("Data saved ");
    })
    .catch(err => {
      res.status(400).send("unable to save to database");
    })
    


});

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index.html');
});
router.get('/insert', function (req, res, next) {
  res.render('../public/reg.html');


});

router.get('/search', function (req, res, next) {
  res.render('../public/search.html');
});
router.get('/gamelist', function (req, res, next) {
  User.find(function (err, data) {
    res.json(data);


  });

})
//Rendering Listing page
router.get('/list', function (req, res, next) {
  res.render('../public/list.html');
})

//Reading data from database
router.get('/findgames', function (req, res, next) {
  global.game=req.param('game');
  console.log('Game Name ' +game);
  var mongoose = require("mongoose");
  mongoose.Promise = global.Promise;
  mongoose.connect("mongodb://localhost:27017/mongodbtest", { useNewUrlParser: true }, function (err, db) {
    if (err) {
      console.log("Error in connecting with db")
    } else {
      User.find({ "game": game }, function (err, data) {
        res.send(data);
        console.log("Search Result:")
        console.log("Game:"+data[0].game);
        console.log("Publisher:"+data[0].publisher);
        console.log("Release Date:"+data[0].released);
        console.log("Score:"+data[0].score);

      })

    }



});


})

//Deleting Record from database after deletion refresh the game list options You can see that record is deleted.
router.get('/delet', function (req, res, next) {
  var id  =req.param('id');
  console.log(id);
  User.remove({ "_id": id }, function (err, data) {
    console.log("Data deleted:" + JSON.stringify(data));
   
   

  });
  
  res.redirect('/list');
});
//Updating records in the database
router.get('/edit', function (req, res, next) {
  global.upid= req.param('id');
  res.redirect('../public/update.html?upid='+upid);

  //console.log(JSON.stringify(data));
  console.log(upid);
  //console.log(ugame);
  //console.log(upublisher);
});

router.post('/updt',function(req,res,next){
var ugame=req.body.ugame;
var upublisher=req.body.upublisher;
console.log(ugame);
console.log(upublisher);
console.log(upid);
User.update({"_id":upid},{$set:{game:ugame,publisher:upublisher}},function(err,data){
  console.log("data updated");
  // res.redirect('/list');
});

});

module.exports = router;